<?php
/**
 * Created by PhpStorm.
 * User: thomascomes
 * Date: 2019-02-01
 * Time: 16:47.
 */

namespace App\Util;

class Calculator
{
    public function add(int $a, int $b): int
    {
        return $a + $b;
    }
}
