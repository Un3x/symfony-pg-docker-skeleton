<?php
/**
 * Created by PhpStorm.
 * User: thomascomes
 * Date: 20/11/2018
 * Time: 11:33.
 */

namespace App\Controller;

use App\Entity\Person;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @Route("/")
     *
     * @return Response
     *
     * @throws \Exception
     */
    public function index()
    {
        $persons = $this->getDoctrine()
                        ->getRepository(Person::class)
                        ->findAll();
        $result = [];

        /** @var Person $person */
        foreach ($persons as $person) {
            $result[] = [
                'email' => $person->getEmail(),
            ];
        }

        return $this->json($result);
    }
}
