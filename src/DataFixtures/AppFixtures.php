<?php

namespace App\DataFixtures;

use App\Entity\Person;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        for ($i = 0; $i < 20; ++$i) {
            $person = new Person();
            $person->setEmail('person '.$i.'-'.random_int(10, 100));
            $manager->persist($person);
        }

        $manager->flush();
    }
}
