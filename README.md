# symfony-pg-skeletton

This is a symfony docker postgresql skeleton.

The goal of this repository is to provide a very easy and quickly functional application ready for development on any machines, with CI process, code quality, basic tooling for deployment.

## Requirements

All the processes used in this application require only the installation of docker and docker-compose.
If you are a UNIX/Ubuntu user, make sure that you do not use docker commands as 'sudo', instead, you should follow the post installations process to give your users the correct rights.

## Installation

Installation process is very simple :
Simply run in your favorite console / terminal :

* make docker start
>It should build the application and get all the tooling required for the project to run.
* make docker.ssh
>It will connect you to the docker php machine
* composer install
> This will install all the project dependencies.
>You can see the project working by going on localhost:8080 with any web browser.

This is it, everything is up.

## Stopping the server

If you want your server to stop, simply run :

make docker.stop
> Do not run this command within the docker container :O SOOO METAA

## Connecting to the database

To access the database you can use :

* make docker.db
>This will connect you to the docker containing the database
* psql -U docker
>This will connect you to the database within the docker container.

## TroubleShooting

I haven't encountered any issues for now with this project, if you do, please let me know so I'll be able to find a way arround.

## Special Thanks 

I have nothing to be thankful for.
