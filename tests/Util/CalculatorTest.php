<?php
/**
 * Created by PhpStorm.
 * User: thomascomes
 * Date: 2019-02-01
 * Time: 16:48
 */

namespace App\Tests\Util;

use App\Util\Calculator;
use PHPUnit\Framework\TestCase;


class CalculatorTest extends TestCase
{
    public function testAdd(): void
    {
        $calculator = new Calculator();
        $this->assertEquals(
            $calculator->add(3, 2),
            5
        );
    }

}