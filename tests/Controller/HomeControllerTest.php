<?php
/**
 * Created by PhpStorm.
 * User: thomascomes
 * Date: 2019-02-01
 * Time: 16:51.
 */

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class HomeControllerTest extends WebTestCase
{
    public function testIndex(): void
    {
        $client = static::createClient();
        $client->request('GET', '/');
        $response = $client->getResponse();

        $payload = json_decode($response->getContent(), true);

        $this->assertEquals(
            200,
            $response->getStatusCode()
        );

        $this->assertEquals(
            \count($payload),
            20
        );
    }
}
