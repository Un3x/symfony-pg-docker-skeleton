##
## ----------------------------------
## Available make target for pg-sf-symfony-skeletton
## ----------------------------------
##

COLOR=\033[0;32m
NC=\033[0m
BOLD=\033[1m

-include .env

COMPOSE_CMD=docker-compose
UNAME=$(shell uname -s)
DOCKER_RUN_PHP=DOCKER_USER_ID=1 ${COMPOSE_CMD} run --rm php
DOCKER_RUN_POSTGRES=DOCKER_USER_ID=1 ${COMPOSE_CMD} run --rm postgres

.DEFAULT_GOAL := help

#
# Common
# ------
#

docker.start: ## Start and build the docker
	${COMPOSE_CMD} up --build -d
	${DOCKER_RUN_PHP} composer install --no-interaction

docker.stop: ## Stop the docker
	${COMPOSE_CMD} down


docker.ssh: ## Connect to the php docker container
	docker exec -i -t symfony-pg-docker-skeleton_php_1 /bin/bash

docker.db: ## Connect to the db docker container
	docker exec -i -t symfony-pg-docker-skeleton_postgres_1 /bin/bash

docker.db.reset: ## Reset the database
	${DOCKER_RUN_PHP} ./bin/console doctrine:schema:drop --force
	${DOCKER_RUN_PHP} ./bin/console doctrine:schema:create

docker.migrate: ## run sf4 migrations
	${DOCKER_RUN_PHP} ./bin/console d:m:m

docker.clearcache: ## clear sf4 cache
	${DOCKER_RUN_PHP} ./bin/console c:c

test: ##running unit test
	${DOCKER_RUN_PHP} php ./bin/phpunit

analyze: ## Analyze project with PHPStan
	${DOCKER_RUN_PHP} ./vendor/bin/phpstan analyze src --level 7 -c phpstan.neon

lint: ## Linting the file
	${DOCKER_RUN_PHP} ./vendor/bin/php-cs-fixer fix src/ --using-cache=no

load.fixtures: ##loading fixtures of the application
	${DOCKER_RUN_PHP} bin/console doctrine:fixtures:load

help:           ## Show this help.
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

